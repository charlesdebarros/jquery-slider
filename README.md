### Simple Picture Slide built with jQuery

This is a simple picture slider as an experiment with jQuery code.

Each slide moves after 3 seconds.

On mouse over, the slideshow will pause.

Once mouse of moved off, the slideshow will restart. 

### Screenshot

![image](https://raw.githubusercontent.com/charlesdebarros/jQuery-Slider/master/images/screenshot.png)

### Technologies used:

* HTML
* CSS
* JavaScript
* jQuery 2.1.3

### TODO

* Make arrows move slides back and forth - at the moment arrows are inactive.
* Style page further
* Make it responsive
* Add different sets of pictures for different topics